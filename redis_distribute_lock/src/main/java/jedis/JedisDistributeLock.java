package jedis;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.UUID;

public class JedisDistributeLock {

    public static final String redisLockPrefix = "redis:lock:";

    /**
     *
     * @param lockName 锁名
     * @param acquireTimeOut  获取锁的超时时间
     * @param lockTimeOut   锁的超时时间
     *
     * @return
     */
    public String getRedisLock(String lockName , Long acquireTimeOut ,Long lockTimeOut){

        String redisLockKey = redisLockPrefix + lockName;
        String idValue = UUID.randomUUID().toString();

        Jedis jedis = JedisPoolInstance.getJedisPoolInstance().getResource();

        try {
            Long endTime = System.currentTimeMillis() + acquireTimeOut;

            while (System.currentTimeMillis() < endTime){
                if(jedis.setnx(redisLockKey,idValue) == 1){
                    jedis.pexpire(redisLockKey,lockTimeOut);
                    return idValue;
                }
                if(jedis.ttl(redisLockKey) == -1){
                    jedis.pexpire(redisLockKey,lockTimeOut);
                }
            }
        }finally {
            if(jedis != null)   jedis.close();
        }
        return null;
    }
    public void releaseRedisLock(String lockName, String uniqueValue) {
        //redis锁的key
        String redisLockKey = redisLockPrefix + lockName;

        Jedis jedis = JedisPoolInstance.getJedisPoolInstance().getResource();

        try {
            //自己的锁自己解，不要把别人的锁给解了
            if (jedis.get(redisLockKey).equals(uniqueValue)) {
                jedis.del(redisLockKey);
            }
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }
}
